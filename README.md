# Balanced Bracket


## Getting started
Cara menjalankan aplikasi ini pada terminal ketik perintah

```go run main.go```

atau bisa menjalankan aplikasi yang sudah di build pada platform masing-masing.

lalu isi input sesuai yang anda inginkan

## Kompleksitas
Aplikasi ini menggunakan algoritma stack.

- **Kompleksitas Waktu**: O(n), di mana n adalah panjang dari string masukan. Hanya menggunakan satu fungsi dan mencari bracket tidak sesuai terlebih dahulu.
- **Kompleksitas Ruang**: O(n), karena kita menggunakan stack yang berkembang seiring dengan panjang string masukan.

```go
// Fungsi untuk memeriksa keseimbangan tanda kurung
func isBalanced(inputStr string) string {
    stack := []rune{}  // Inisialisasi stack kosong
    bracketPairs := map[rune]rune{'(': ')', '[': ']', '{': '}'}

    for _, char := range inputStr {
        // Jika karakter adalah tanda kurung terbuka, tambahkan ke stack
        if char == '(' || char == '[' || char == '{' {
            stack = append(stack, char)
        // Jika karakter adalah tanda kurung penutup
        } else if char == ')' || char == ']' || char == '}' {
            // Jika stack kosong, ekspresi tidak seimbang
            if len(stack) == 0 {
                return "NO"
            }
            // Ambil tanda kurung teratas dari stack
            top := stack[len(stack)-1]
            // Hapus tanda kurung teratas dari stack
            stack = stack[:len(stack)-1]
            // Jika tanda kurung penutup tidak cocok dengan tanda kurung teratas, maka ekspresi tidak seimbang
            if bracketPairs[top] != char {
                return "NO"
            }
        }
    }
    // Jika stack tidak kosong setelah looping selesai, maka ekspresi tidak seimbang
    if len(stack) > 0 {
        return "NO"
    }
    // Jika stack kosong setelah looping selesai, maka ekspresi seimbang
    return "YES"
}

```
### Penjelasan:
- Inisialisasi Stack: Stack diinisialisasi sebagai slice kosong dari rune.
Iterasi Karakter: Setiap karakter dalam string diperiksa satu per satu.
- Push: Jika karakter adalah tanda kurung terbuka, masukkan ke dalam stack.
- Pop dan Periksa: Jika karakter adalah tanda kurung penutup, periksa apakah stack kosong. Jika tidak, keluarkan elemen teratas dari stack dan periksa apakah cocok dengan tanda kurung penutup.
- Cek Akhir: Setelah iterasi selesai, jika stack masih memiliki elemen, berarti ekspresi tidak seimbang.