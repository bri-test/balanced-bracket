package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"time"
)

func isBalanced(inputStr string) string {
	stack := []rune{}
	bracketPairs := map[rune]rune{'(': ')', '[': ']', '{': '}'}

	for _, char := range inputStr {
		if char == '(' || char == '[' || char == '{' {
			stack = append(stack, char)
		} else if char == ')' || char == ']' || char == '}' {
			if len(stack) == 0 {
				return "NO"
			}
			top := stack[len(stack)-1]
			stack = stack[:len(stack)-1]
			if bracketPairs[top] != char {
				return "NO"
			}
		}
	}
	if len(stack) > 0 {
		return "NO"
	}
	return "YES"
}

func RunSample() {
	input1 := "{ [ ( ) ] }"
	fmt.Println("Input:", input1)
	startTime1 := time.Now()
	fmt.Println("Output:", isBalanced(input1))
	elapsedTime1 := time.Since(startTime1)
	fmt.Printf("Waktu eksekusi Sample 1: %v ms\n", elapsedTime1.Seconds()*1000)

	input2 := "{ [ ( ] ) }"
	fmt.Println("Input:", input2)
	startTime2 := time.Now()
	fmt.Println("Output:", isBalanced(input2))
	elapsedTime2 := time.Since(startTime2)
	fmt.Printf("Waktu eksekusi Sample 2: %v ms\n", elapsedTime2.Seconds()*1000)

	input3 := "{ ( ( [ ] ) [ ] ) [ ] }"
	fmt.Println("Input:", input3)
	startTime3 := time.Now()
	fmt.Println("Output:", isBalanced(input3))
	elapsedTime3 := time.Since(startTime3)
	fmt.Printf("Waktu eksekusi Sample 3: %v ms\n", elapsedTime3.Seconds()*1000)
	fmt.Print("\n\n")
}

func main() {
	RunSample()
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("input : ")
	input, _ := reader.ReadString('\n')
	reg, err := regexp.Compile(`[()\{\}\[\]]`)
	if err != nil {
		log.Println("failed compile regex")
		return
	}
	if !reg.MatchString(input) {
		log.Println("hanya menerima karakter  ( , ) , { , } , atau [ , ]")
		return
	}

	startTime := time.Now()
	fmt.Println("Output:", isBalanced(input))
	elapsedTime := time.Since(startTime)
	fmt.Printf("Waktu eksekusi : %v ms\n", elapsedTime.Seconds()*1000)

}
